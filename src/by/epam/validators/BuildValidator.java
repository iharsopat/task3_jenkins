package by.epam.validators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * Task for validation ant name.
 *
 * @author  Ihar Sopat
 */

public class BuildValidator extends Task {
    private static final String NAME = "name";
    private static final String NAME_PATTERN = "^([a-zA-Z]+[-]??[a-zA-Z]+)$";

    private List<BuildFile> buildFiles = new ArrayList<>();
    private boolean checkNames = false;

    /**
     * Constructs an empty BuildValidator.
     */
    public BuildValidator() {

    }

    /**
     * Constructs BuildValidator with parameter.
     * @param  checkNames It's check name flag
     */
    public BuildValidator(final boolean checkNames) {
        this.checkNames = checkNames;
    }

    /**
     * Setter for check name flag.
     * @param  checkNames It's new check name flag
     */
    public void setCheckNames(final boolean checkNames) {
        this.checkNames = checkNames;
    }
    /**
     * Special method for BuildFile creation
     * @return  BuildFile
     */
    public BuildFile createBuildFile() {
        BuildFile buildFile = new BuildFile();
        buildFiles.add(buildFile);
        return buildFile;
    }
    /**
     * Inner class BuildFile.
     */
    public static class BuildFile {
        private String location;

        /**
         * @return  File name of BuildFile.
         */
        public String getLocation() {
            return location;
        }

        /**
         * @param  location File name of BuildFile.
         */
        public void setLocation(final String location) {
            this.location = location;
        }
    }

    @Override
    public void execute() throws BuildException {

        for (BuildFile buildFile : buildFiles) {
            String fileName = buildFile.getLocation();
            XMLInputFactory factory = XMLInputFactory.newInstance();

            FileInputStream fis = null;
            XMLEventReader reader = null;

            try {
                fis = new FileInputStream(fileName);
                reader = factory.createXMLEventReader(fis);
                while (reader.hasNext()) {
                    XMLEvent event = reader.nextEvent();
                    if (event.getEventType() == XMLStreamConstants.START_ELEMENT) {
                        StartElement startElement = event.asStartElement();
                        Iterator<Attribute> iterator = startElement.getAttributes();
                        while (iterator.hasNext()) {
                            Attribute attribute = iterator.next();
                            String name = attribute.getName().toString();
                            if (NAME.equalsIgnoreCase(name)) {
                                String value = attribute.getValue();
                                if (!nameIsCorrect(value)) {
                                    throw new BuildException("Buildfile " + fileName + " validation failed. Value "
                                            + value + " is not correct.");
                                }
                            }
                        }
                    }
                }
            } catch (XMLStreamException e) {
                throw new BuildException(e.getMessage());
            } catch (FileNotFoundException e) {
                throw new BuildException(e.getMessage());
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (XMLStreamException e) {
                        throw new BuildException(e.getMessage());
                    }
                }
            }
        }
    }


    private static boolean nameIsCorrect(final String name) {
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

}
